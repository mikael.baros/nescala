package nes

import memory._
import memory.MemoryImplicits._
import cpu._

object Main extends App {
  val mem: MemoryBlock = new NesMemoryBlock()
  mem.write(0, 123)
  mem.write(0x2000, 1)
  mem.write(0x2001, 2)
  mem.write(0x2002, 3)
  mem.write(0x2003, 4)
  mem.write(0x2004, 5)
  mem.write(0x2005, 6)
  mem.write(0x2006, 7)
  mem.write(0x2007, 8)
  println(mem.read(0))
  println(mem.read(0x0800))
  println(mem.read(0x2008))
  println(mem.read(0x2009))
  println(mem.read(0x200A))
  println(mem.read(0x200B))
  println(mem.read(0x200C))
  println(mem.read(0x200D))
  println(mem.read(0x200E))
  println(mem.read(0x200F))

  val cpu: Cpu = new Cpu()

  println("Status register")

  cpu.p.C = 1
  println(cpu.p.C)
  println(cpu.p.value)
  cpu.p.I = 1
  println(cpu.p.I)
  println(cpu.p.value)
  cpu.p.I = 0
  println(cpu.p.I)
  println(cpu.p.value)
  cpu.p.C = 0
  println(cpu.p.C)
  println(cpu.p.value)
}
