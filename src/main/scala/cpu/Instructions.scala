package cpu

import memory.MemoryImplicits._

// I'll gather all instruction logic here, but will try to generalize logic across various calls as much as possible
object InstructionLogic {
  // General curried version of setting a CPU register's value from memory. Used for LDA, LDX and LDY
  def loadRegisterLogic(register: CpuRegister)(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.setRegister(register, cpu.memoryBlock.read(parameterInfo.address))
    0
  }

  // General curried version of storing a register's value to a location in memory. Used for STA, STX and STY
  def writeRegisterLogic(register: CpuRegister)(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.memoryBlock.write(parameterInfo.address, cpu.getRegister(register))
    0
  }

  // The most basic status register management, check for Zero and check for bit 7
  def statusRegisterZN(cpu: Cpu, value: Short): Unit = {
    if (value == 0)
      cpu.p.Z = 1
    cpu.p.N = (value & 128).toByte
  }

  // Method that simplifies conditionally storing a result in the accumulator, or a memory location
  // used by a couple instructions
  def storeValue(cpu: Cpu, value: Short, address: Int, storeInAccumulator: Boolean): Unit = {
    if (storeInAccumulator)
      cpu.acc = value
    else
      cpu.memoryBlock.write(address, value)
  }

  // Add logic
  def ADC(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.acc = (cpu.acc + parameterInfo.value + cpu.p.C).toShort
    0
  }

  // Subtraction logic
  def SBC(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.acc = (cpu.acc - parameterInfo.value - ~cpu.p.C).toShort
    0
  }

  // General curried version for setting the value of the C flag of CPU.p. Used for CLC and SEC
  def setC(value: Byte)(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.p.C = value
    0
  }

  // Logical AND
  def AND(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.acc = (cpu.acc & parameterInfo.value).toShort
    statusRegisterZN(cpu, cpu.acc)
    0
  }

  // Combined curried ASL_LSR to reduce code
  def ASL_LSR(storeInAccumulator: Boolean, shiftLeft: Boolean)(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    val result: Short = if (shiftLeft) (parameterInfo.value << 1).toShort else (parameterInfo.value >>> 1).toByte

    storeValue(cpu, result, parameterInfo.address, storeInAccumulator)

    cpu.p.C = if (shiftLeft) (result & 128).toByte else (result & 0x1).toByte

    statusRegisterZN(cpu, result)

    0
  }

  // The bit test instruction (basically only a CPU status flag setter)
  def BIT(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.p.Z = if ((cpu.acc & parameterInfo.value) == 0) 1 else 0
    cpu.p.O = (parameterInfo.value & 64).toByte
    cpu.p.N = (parameterInfo.value & 128).toByte
    0
  }

  // Exclusive or (XOR)
  def EOR(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.acc = (cpu.acc ^ parameterInfo.value).toByte
    statusRegisterZN(cpu, cpu.acc)
    0
  }

  // Logical OR
  def ORA(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.acc = (cpu.acc | parameterInfo.value).toByte
    statusRegisterZN(cpu, cpu.acc)
    0
  }

  // Combined curried version of ROL and ROR, again to reduce code as the only difference between the two
  // is the single line of rotation
  def ROL_ROR(storeInAccumulator: Boolean, rotateLeft: Boolean)(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    val result: Short = (if (rotateLeft) (parameterInfo.value << 1) | (parameterInfo.value >>> 7) else (parameterInfo.value >>> 1) | (parameterInfo.value << 7)).toShort
    statusRegisterZN(cpu, result)
    cpu.p.C = (parameterInfo.value & 128).toByte
    storeValue(cpu, result, parameterInfo.address, storeInAccumulator)
    0
  }

  def JMP(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.programCounter = parameterInfo.value
    0
  }

  def JSR(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    val returnPoint = instructionAddress + 3 // JSR is always three bytes, but this is a terrible hack
    cpu.stack.push((returnPoint - 1).toShort)
    cpu.programCounter = parameterInfo.value
    0
  }

  def RTS(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    cpu.programCounter = cpu.stack.pop() + 1
    0
  }

  def branch(flag: FlagName, branchIf: Byte)(cpu: Cpu, instructionAddress: Int, parameterInfo: Parameter): Int = {
    if(cpu.p.getFlag(flag) == branchIf) {
      cpu.programCounter = cpu.programCounter + parameterInfo.value
      1 + parameterInfo.extraCycles // Extra cycles are doubled in a branch so we apply them here too
    } else {
      0
    }
  }
}

// The object containing a map of all instructions, mapped on their opcodes.
object Instructions {
  // Simple map of the OpCode and the actual instruction
  var map: Map[Byte, Instruction] = Map()

  // Helper function to add an instruction variant to the map above
  def addInstruction(name: String, opCode: Byte, addressingMode: AddressingMode, cycles: Int, instructionLogic: (Cpu, Int, Parameter) => Int): Unit = {
    map = map + (opCode -> Instruction(name, opCode, addressingMode, cycles, instructionLogic))
  }

  // Not going to comment this huge block of dumb boilerplate, you get it
  addInstruction("LDA", 0xA9.toByte, Immediate, 2, InstructionLogic.loadRegisterLogic(Acc))
  addInstruction("LDA", 0xA5.toByte, ZeroPage, 3, InstructionLogic.loadRegisterLogic(Acc))
  addInstruction("LDA", 0xB5.toByte, ZeroPageX, 4, InstructionLogic.loadRegisterLogic(Acc))
  addInstruction("LDA", 0xAD.toByte, Absolute, 4, InstructionLogic.loadRegisterLogic(Acc))
  addInstruction("LDA", 0xBD.toByte, AbsoluteX, 4, InstructionLogic.loadRegisterLogic(Acc))
  addInstruction("LDA", 0xB9.toByte, AbsoluteY, 4, InstructionLogic.loadRegisterLogic(Acc))
  addInstruction("LDA", 0xA1.toByte, IndexedIndirect, 6, InstructionLogic.loadRegisterLogic(Acc))
  addInstruction("LDA", 0xB1.toByte, IndirectIndexed, 5, InstructionLogic.loadRegisterLogic(Acc))

  addInstruction("LDX", 0xA2.toByte, Immediate, 2, InstructionLogic.loadRegisterLogic(X))
  addInstruction("LDX", 0xA6.toByte, ZeroPage, 3, InstructionLogic.loadRegisterLogic(X))
  addInstruction("LDX", 0xB6.toByte, ZeroPageY, 4, InstructionLogic.loadRegisterLogic(X))
  addInstruction("LDX", 0xAE.toByte, Absolute, 4, InstructionLogic.loadRegisterLogic(X))
  addInstruction("LDX", 0xBE.toByte, AbsoluteY, 4, InstructionLogic.loadRegisterLogic(X))

  addInstruction("LDY", 0xA0.toByte, Immediate, 2, InstructionLogic.loadRegisterLogic(Y))
  addInstruction("LDY", 0xA4.toByte, ZeroPage, 3, InstructionLogic.loadRegisterLogic(Y))
  addInstruction("LDY", 0xB4.toByte, ZeroPageX, 4, InstructionLogic.loadRegisterLogic(Y))
  addInstruction("LDY", 0xAC.toByte, Absolute, 4, InstructionLogic.loadRegisterLogic(Y))
  addInstruction("LDY", 0xBC.toByte, AbsoluteX, 4, InstructionLogic.loadRegisterLogic(Y))

  addInstruction("STA", 0x85.toByte, ZeroPage, 3, InstructionLogic.writeRegisterLogic(Acc))
  addInstruction("STA", 0x95.toByte, ZeroPageX, 4, InstructionLogic.writeRegisterLogic(Acc))
  addInstruction("STA", 0x8D.toByte, Absolute, 4, InstructionLogic.writeRegisterLogic(Acc))
  addInstruction("STA", 0x9D.toByte, AbsoluteX, 4, InstructionLogic.writeRegisterLogic(Acc))
  addInstruction("STA", 0x99.toByte, AbsoluteY, 4, InstructionLogic.writeRegisterLogic(Acc))
  addInstruction("STA", 0x81.toByte, IndexedIndirect, 6, InstructionLogic.writeRegisterLogic(Acc))
  addInstruction("STA", 0x91.toByte, IndirectIndexed, 5, InstructionLogic.writeRegisterLogic(Acc))

  addInstruction("STX", 0x86.toByte, ZeroPage, 3, InstructionLogic.writeRegisterLogic(X))
  addInstruction("STX", 0x96.toByte, ZeroPageY, 4, InstructionLogic.writeRegisterLogic(X))
  addInstruction("STX", 0x8E.toByte, Absolute, 4, InstructionLogic.writeRegisterLogic(X))

  addInstruction("STY", 0x84.toByte, ZeroPage, 3, InstructionLogic.writeRegisterLogic(Y))
  addInstruction("STY", 0x94.toByte, ZeroPageX, 4, InstructionLogic.writeRegisterLogic(Y))
  addInstruction("STY", 0x8C.toByte, Absolute, 4, InstructionLogic.writeRegisterLogic(Y))

  addInstruction("ADC", 0x69.toByte, Immediate, 2, InstructionLogic.ADC)
  addInstruction("ADC", 0x65.toByte, ZeroPage, 3, InstructionLogic.ADC)
  addInstruction("ADC", 0x75.toByte, ZeroPageX, 4, InstructionLogic.ADC)
  addInstruction("ADC", 0x6D.toByte, Absolute, 4, InstructionLogic.ADC)
  addInstruction("ADC", 0x7D.toByte, AbsoluteX, 4, InstructionLogic.ADC)
  addInstruction("ADC", 0x79.toByte, AbsoluteY, 4, InstructionLogic.ADC)
  addInstruction("ADC", 0x61.toByte, IndexedIndirect, 6, InstructionLogic.ADC)
  addInstruction("ADC", 0x71.toByte, IndirectIndexed, 5, InstructionLogic.ADC)

  addInstruction("SBC", 0xE9.toByte, Immediate, 2, InstructionLogic.SBC)
  addInstruction("SBC", 0xE5.toByte, ZeroPage, 3, InstructionLogic.SBC)
  addInstruction("SBC", 0xF5.toByte, ZeroPageX, 4, InstructionLogic.SBC)
  addInstruction("SBC", 0xED.toByte, Absolute, 4, InstructionLogic.SBC)
  addInstruction("SBC", 0xFD.toByte, AbsoluteX, 4, InstructionLogic.SBC)
  addInstruction("SBC", 0xF9.toByte, AbsoluteY, 4, InstructionLogic.SBC)
  addInstruction("SBC", 0xE1.toByte, IndexedIndirect, 6, InstructionLogic.SBC)
  addInstruction("SBC", 0xF1.toByte, IndirectIndexed, 5, InstructionLogic.SBC)

  addInstruction("CLC", 0x18.toByte, Immediate, 2, InstructionLogic.setC(0))
  addInstruction("SEC", 0x38.toByte, Immediate, 2, InstructionLogic.setC(1))


  addInstruction("AND", 0x29.toByte, Immediate, 2, InstructionLogic.AND)
  addInstruction("AND", 0x25.toByte, ZeroPage, 3, InstructionLogic.AND)
  addInstruction("AND", 0x35.toByte, ZeroPageX, 4, InstructionLogic.AND)
  addInstruction("AND", 0x2D.toByte, Absolute, 4, InstructionLogic.AND)
  addInstruction("AND", 0x3D.toByte, AbsoluteX, 4, InstructionLogic.AND)
  addInstruction("AND", 0x39.toByte, AbsoluteY, 4, InstructionLogic.AND)
  addInstruction("AND", 0x21.toByte, IndexedIndirect, 6, InstructionLogic.AND)
  addInstruction("AND", 0x31.toByte, IndirectIndexed, 5, InstructionLogic.AND)

  addInstruction("ASL", 0x0A.toByte, Accumulator, 2, InstructionLogic.ASL_LSR(storeInAccumulator = true, shiftLeft = true))
  addInstruction("ASL", 0x06.toByte, ZeroPage, 5, InstructionLogic.ASL_LSR(storeInAccumulator = false, shiftLeft = true))
  addInstruction("ASL", 0x16.toByte, ZeroPageX, 6, InstructionLogic.ASL_LSR(storeInAccumulator = false, shiftLeft = true))
  addInstruction("ASL", 0x0E.toByte, Absolute, 6, InstructionLogic.ASL_LSR(storeInAccumulator = false, shiftLeft = true))
  addInstruction("ASL", 0x1E.toByte, AbsoluteX, 7, InstructionLogic.ASL_LSR(storeInAccumulator = false, shiftLeft = true))

  addInstruction("LSR", 0x4A.toByte, Accumulator, 2, InstructionLogic.ASL_LSR(storeInAccumulator = true, shiftLeft = false))
  addInstruction("LSR", 0x46.toByte, ZeroPage, 5, InstructionLogic.ASL_LSR(storeInAccumulator = false, shiftLeft = false))
  addInstruction("LSR", 0x56.toByte, ZeroPageX, 6, InstructionLogic.ASL_LSR(storeInAccumulator = false, shiftLeft = false))
  addInstruction("LSR", 0x4E.toByte, Absolute, 6, InstructionLogic.ASL_LSR(storeInAccumulator = false, shiftLeft = false))
  addInstruction("LSR", 0x5E.toByte, AbsoluteX, 7, InstructionLogic.ASL_LSR(storeInAccumulator = false, shiftLeft = false))

  addInstruction("BIT", 0x24.toByte, ZeroPage, 3, InstructionLogic.BIT)
  addInstruction("BIT", 0x2C.toByte, Absolute, 4, InstructionLogic.BIT)

  addInstruction("EOR", 0x49.toByte, Immediate, 2, InstructionLogic.EOR)
  addInstruction("EOR", 0x45.toByte, ZeroPage, 3, InstructionLogic.EOR)
  addInstruction("EOR", 0x55.toByte, ZeroPageX, 4, InstructionLogic.EOR)
  addInstruction("EOR", 0x4D.toByte, Absolute, 4, InstructionLogic.EOR)
  addInstruction("EOR", 0x5D.toByte, AbsoluteX, 4, InstructionLogic.EOR)
  addInstruction("EOR", 0x59.toByte, AbsoluteY, 4, InstructionLogic.EOR)
  addInstruction("EOR", 0x41.toByte, IndexedIndirect, 6, InstructionLogic.EOR)
  addInstruction("EOR", 0x51.toByte, IndirectIndexed, 5, InstructionLogic.EOR)

  addInstruction("ROL", 0x2A.toByte, Accumulator, 2, InstructionLogic.ROL_ROR(storeInAccumulator = true, rotateLeft = true))
  addInstruction("ROL", 0x26.toByte, ZeroPage, 5, InstructionLogic.ROL_ROR(storeInAccumulator = false, rotateLeft = true))
  addInstruction("ROL", 0x36.toByte, ZeroPageX, 6, InstructionLogic.ROL_ROR(storeInAccumulator = false, rotateLeft = true))
  addInstruction("ROL", 0x2E.toByte, Absolute, 6, InstructionLogic.ROL_ROR(storeInAccumulator = false, rotateLeft = true))
  addInstruction("ROL", 0x3E.toByte, AbsoluteX, 7, InstructionLogic.ROL_ROR(storeInAccumulator = false, rotateLeft = true))

  addInstruction("ROR", 0x6A.toByte, Accumulator, 2, InstructionLogic.ROL_ROR(storeInAccumulator = true, rotateLeft = false))
  addInstruction("ROR", 0x66.toByte, ZeroPage, 5, InstructionLogic.ROL_ROR(storeInAccumulator = false, rotateLeft = false))
  addInstruction("ROR", 0x76.toByte, ZeroPageX, 6, InstructionLogic.ROL_ROR(storeInAccumulator = false, rotateLeft = false))
  addInstruction("ROR", 0x6E.toByte, Absolute, 6, InstructionLogic.ROL_ROR(storeInAccumulator = false, rotateLeft = false))
  addInstruction("ROR", 0x7E.toByte, AbsoluteX, 7, InstructionLogic.ROL_ROR(storeInAccumulator = false, rotateLeft = false))

  addInstruction("JMP", 0x4C.toByte, Absolute, 3, InstructionLogic.JMP)
  addInstruction("JMP", 0x6C.toByte, Indirect, 5, InstructionLogic.JMP)

  addInstruction("JSR", 0x4C.toByte, Absolute, 6, InstructionLogic.JSR)

  addInstruction("RTS", 0x60.toByte, Implicit, 6, InstructionLogic.RTS)

  addInstruction("BCC", 0x90.toByte, Relative, 2, InstructionLogic.branch(Carry, branchIf = 0))
  addInstruction("BCS", 0xB0.toByte, Relative, 2, InstructionLogic.branch(Carry, branchIf = 1))

  addInstruction("BNE", 0xD0.toByte, Relative, 2, InstructionLogic.branch(Zero, branchIf = 0))
  addInstruction("BEQ", 0xF0.toByte, Relative, 2, InstructionLogic.branch(Zero, branchIf = 1))

  addInstruction("BPL", 0x10.toByte, Relative, 2, InstructionLogic.branch(Negative, branchIf = 0))
  addInstruction("BMI", 0x30.toByte, Relative, 2, InstructionLogic.branch(Negative, branchIf = 1))

  addInstruction("BVC", 0x50.toByte, Relative, 2, InstructionLogic.branch(Overflow, branchIf = 0))
  addInstruction("BVS", 0x70.toByte, Relative, 2, InstructionLogic.branch(Overflow, branchIf = 1))
}
