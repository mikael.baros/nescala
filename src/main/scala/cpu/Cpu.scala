package cpu

import memory._
import memory.MemoryImplicits._

sealed trait FlagName { val bit: Byte }
case object Carry extends FlagName { val bit: Byte = 0 }
case object Zero extends FlagName { val bit: Byte = 1 }
case object Interrupt extends FlagName { val bit: Byte = 2 }
case object Decimal extends FlagName { val bit: Byte = 3 }
case object Break extends FlagName { val bit: Byte = 4 }
case object Overflow extends FlagName { val bit: Byte = 6 }
case object Negative extends FlagName { val bit: Byte = 7 }

class P {
  var value: Byte = 0

  private def getBit(n: Byte): Byte = {
    ((value >>> n) & 0x1).toByte
  }

  private def toggleBit(n: Byte): Unit = {
    value = (value ^ (1 << n)).toByte
  }

  private def setBit(n: Byte, v: Byte): Unit = {
    if(getBit(n) != v)
      toggleBit(n)
  }

  def setFlag(flag: FlagName, value: Byte): Unit = {
    setBit(flag.bit, value)
  }

  def getFlag(flag: FlagName): Byte = {
    getBit(flag.bit)
  }

  def C: Byte = getBit(0)
  def C_= (c:Byte): Unit = setBit(0, c)

  def Z: Byte = getBit(1)
  def Z_= (z:Byte): Unit = setBit(1, z)

  def I: Byte = getBit(2)
  def I_= (i:Byte): Unit = setBit(2, i)

  def D: Byte = getBit(3)
  def D_= (d:Byte): Unit = setBit(3, d)

  def B: Byte = getBit(4)
  def B_= (b:Byte): Unit = setBit(4, b)

  def U: Byte = 1
  def U_= (u:Byte): Unit = setBit(4, u)

  def O: Byte = getBit(6)
  def O_= (o:Byte): Unit = setBit(6, o)

  def N: Byte = getBit(7)
  def N_= (n:Byte): Unit = setBit(7, n)
}

sealed trait CpuRegister
case object X extends CpuRegister
case object Y extends CpuRegister
case object Acc extends CpuRegister

class Cpu {
  // The CPU's memory block
  val memoryBlock: MemoryBlock = new NesMemoryBlock()

  // The simple one-byte registers represented as Shorts due to no unsigned support
  var x: Short = 0
  var y: Short = 0
  var acc: Short = 0

  // The status register
  val p: P = new P()

  // The program counter pointing to instructions
  var programCounter: Int = 0

  // The stack, represented in the memory block
  val stack: Stack = new Stack(0x01FF, memoryBlock)

  def setRegister(register: CpuRegister, value: Short): Unit = register match {
    case X => x = value
    case Y => y = value
    case Acc => acc = value
  }

  def getRegister(register: CpuRegister): Short = register match {
    case X => x
    case Y => y
    case Acc => acc
  }

  def runInstruction(): Unit = {
    val instruction = Instructions.map((memoryBlock.read(programCounter) & 0xFF).toByte)
    val curPC = programCounter
    instruction.execute(this, memoryBlock, programCounter)
    if(curPC == programCounter)
      programCounter += instruction.byteSize
  }
}
