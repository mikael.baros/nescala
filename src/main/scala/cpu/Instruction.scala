package cpu

import memory._
import memory.MemoryImplicits._

// All the different Addressing Modes on the NES CPU
sealed trait AddressingMode
case object Implicit extends AddressingMode
case object Accumulator extends AddressingMode
case object Immediate extends AddressingMode
case object ZeroPage extends AddressingMode
case object ZeroPageX extends AddressingMode
case object ZeroPageY extends AddressingMode
case object Relative extends AddressingMode
case object Absolute extends AddressingMode
case object AbsoluteX extends AddressingMode
case object AbsoluteY extends AddressingMode
case object Indirect extends AddressingMode
case object IndexedIndirect extends AddressingMode
case object IndirectIndexed extends AddressingMode

// The main parameter to an instruction logic function containing the actual parameter value, the address it was read from, and if
// the memory lookup consumed any extra cycles
case class Parameter(value: Int, address: Int, extraCycles: Int)

// The instruction class, containing all code for looking up parameters in memory, in order to send it to the encapsulated logic
case class Instruction(name: String, opCode: Byte, addressingMode: AddressingMode, cycles: Int, instructionLogic: (Cpu, Int, Parameter) => Int) {
  // Amount of bytes this instruction occupies in memory
  // Used by the CPU (later) to find out where the next instruction is located (so we can keep running the game)
  var byteSize: Byte = addressingMode match {
    case Implicit | Accumulator => 1
    case Immediate | ZeroPage | ZeroPageX | ZeroPageY | IndexedIndirect | IndirectIndexed | Relative => 2
    case Absolute | AbsoluteX | AbsoluteY | Indirect => 3
  }

  // Fetch a two-byte value instead of a single-byte value, used by about half of the addressing modes
  private def highParam(memory: MemoryBlock, lowAddress: Int): Int = {
    memory.read(lowAddress) | (memory.read(lowAddress + 1) << 8)
  }

  // If crossing a page (from 0xXXFF to anything after it) the instruction uses an extra cycle, only used for two addressing modes
  // This is a must if you want pixel-perfect graphics emulation later on, but can be skipped
  private def calculatePageCrossedPenalty(addressBefore: Int, addressAfter: Int): Int = {
    if ((addressAfter & 0xFF00) != (addressBefore & 0xFF00))
      1
    else
      0
  }

  // Runs instruction, returns the number of CPU cycles consumed (cycles are used later in graphics programming :) )
  def execute(cpu: Cpu, memory: MemoryBlock, startingAddress: Int): Int = {
    // The first parameter (if any) for this instruction starts at one byte from the instruction's address
    val lowAddress = startingAddress + 1

    // Resolve the addressing mode and read the actual parameter
    val parameter: Parameter = addressingMode match {
      case Implicit => Parameter(0, 0, 0)
      case Immediate => Parameter(memory.read(lowAddress), lowAddress, 0)
      case ZeroPage =>
        val address: Int = memory.read(lowAddress)
        Parameter(memory.read(address), address, 0)
      case ZeroPageX =>
        val address: Int = memory.read(lowAddress) + cpu.x
        Parameter(memory.read(address & 0xFF), address, 0)
      case ZeroPageY =>
        val address: Int = memory.read(lowAddress) + cpu.y
        Parameter(memory.read(address), address, 0)
      case Absolute =>
        val address: Int = highParam(memory, lowAddress)
        Parameter(memory.read(address), address, 0)
      case AbsoluteX =>
        val address: Int = highParam(memory, lowAddress) + cpu.x
        val value: Int = memory.read(address)
        Parameter(value, address, calculatePageCrossedPenalty(value, address))
      case AbsoluteY =>
        val address: Int = highParam(memory, lowAddress) + cpu.y
        val value: Int = memory.read(address)
        Parameter(value, address, calculatePageCrossedPenalty(value, address))
      case Relative =>
        val value: Int = memory.read(lowAddress)
        Parameter(value, startingAddress + value, 0)
      case Accumulator => Parameter(cpu.acc, lowAddress, 0)
      case Indirect =>
        val addressLow: Int = highParam(memory, lowAddress)
        val valueLow: Int = memory.read(addressLow)

        // Weird JMP bug on the very first NES CPU http://obelisk.me.uk/6502/reference.html#JMP
        var addressHigh = addressLow + 1
        if ((addressLow & 0xFF) == 0xFF) {
          addressHigh = addressLow & 0xFF00
        }

        val valueHigh: Int = memory.read(addressHigh)

        Parameter((valueHigh << 8) | valueLow, addressLow, 0)
      case IndexedIndirect =>
        val value = memory.read(lowAddress) + cpu.x
        val extraCycles = calculatePageCrossedPenalty(lowAddress, value)
        val valueLow = memory.read(value & 0xFF)
        val valueHigh = memory.read((value + 1) & 0xFF)
        val valueTrue = memory.read((valueHigh << 8) | valueLow)
        Parameter(valueTrue, (valueHigh << 8) | valueLow, extraCycles)
      case IndirectIndexed =>
        val value = memory.read(lowAddress)
        val valueLow = memory.read(value & 0xFF)
        val valueHigh = memory.read((value + 1) & 0xFF)
        val addressTrue = ((valueHigh << 8) | valueLow) + 1
        val valueTrue = memory.read(addressTrue)
        val extraCycles = calculatePageCrossedPenalty((valueHigh << 8) | valueLow, addressTrue)
        Parameter(valueTrue, addressTrue, extraCycles)
    }

    // Finally, run the encapsulated logic, supply it the resolved parameter, and returns the number of cycles consumed
    // as a sum of the cycles, any extra cycles in memory lookup, and if the instruction itself incurs any extra cycles
    cycles + parameter.extraCycles + instructionLogic(cpu, startingAddress, parameter)
  }
}
