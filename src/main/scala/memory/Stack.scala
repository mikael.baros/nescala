package memory

import memory.MemoryImplicits._

class Stack(var stackPointer: Int, val memoryBlock: MemoryBlock) {
  // Push a byte onto the stack
  def push(value: Short): Unit = {
    memoryBlock.write(stackPointer, value)
    stackPointer -= 1
  }

  // Push a two-byte value onto the stack, used in some places
  def push(value: Int): Unit = {
    push((value >>> 8).toShort)
    push((value & 0xff).toShort)
  }

  // Pop a byte from the stack
  def pop(): Short = {
    stackPointer += 1
    memoryBlock.read(stackPointer)
  }

  // Pop a two-byte value, used in some places in the future
  def popWide(): Int = {
    pop() | (pop() << 8)
  }
}
