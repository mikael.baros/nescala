package memory

import scala.language.implicitConversions

object MemoryImplicits {
  implicit def int2Address(address: Int): MemoryAddress =
    new MemoryAddress(address)
}

class MemoryAddress(address: Int) {
  private val integerAddress: Int = address match {
    case ad if 0x0800 until 0x1FFF contains ad => ad - 0x0800
    case ad if 0x2008 until 0x3FFF contains ad => 0x2000 + (ad % 8)
    case _ => address
  }

  def value:Int = integerAddress
}

trait MemoryBlock {
  def read(address: MemoryAddress): Short
  def write(address: MemoryAddress, value: Short): Unit
}

class NesMemoryBlock extends MemoryBlock {
  val data = new Array[Short](0xFFFF)

  def read(address: MemoryAddress): Short = (data(address.value) & 0xff).toShort

  def write(address: MemoryAddress, value: Short): Unit = data(address.value) = (value & 0xff).toShort
}
